﻿using McHack.Domain;
using Microsoft.EntityFrameworkCore;

namespace McHack.Domain.Data
{
    public class ManufacturerContext : DbContext
    {
        public ManufacturerContext(DbContextOptions<ManufacturerContext> options)
            : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasPostgresExtension("postgis");
        }

        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<WebSiteSetting> WebSiteSettings { get; set; }
    }
}
