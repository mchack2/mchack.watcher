﻿namespace McHack.Domain.Repository
{
    public interface IRepository<T>
    {
        T Add(T item);
        T FindById(int id);
        IEnumerable<T> Get();
        IEnumerable<T> Get(Func<T, bool> predicate);
        void Remove(T item);
        void Update(T item);

    }

}
