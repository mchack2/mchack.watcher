﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McHack.Domain.Repository
{
    public class ManufacturerRepository : Repository<Manufacturer>
    {
        public ManufacturerRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<Manufacturer> GetWithProducts()
        {
            return _dbSet.Include(x => x.Products).ToList();
        }
    }
}
