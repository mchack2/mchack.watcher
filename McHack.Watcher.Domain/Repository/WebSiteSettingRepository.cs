﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McHack.Domain.Repository
{
    public class WebSiteSettingRepository : Repository<WebSiteSetting>
    {
        public WebSiteSettingRepository(DbContext context) : base(context)
        {
        }
    }
}
