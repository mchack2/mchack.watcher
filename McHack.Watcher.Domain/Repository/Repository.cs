﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McHack.Domain.Repository
{
    public class Repository<T> : IRepository<T>
        where T : class
    {
        protected DbContext _context;
        protected DbSet<T> _dbSet;

        public Repository(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }

        public IEnumerable<T> Get()
        {
            return _dbSet;
        }

        public IEnumerable<T> Get(Func<T, bool> predicate)
        {
            return _dbSet.Where(predicate);
        }

        public long Count(Func<T, bool> predicate)
        {
            return _dbSet.LongCount(predicate);
        }

        public T FindById(int id)
        {
            return _dbSet.Find(id);
        }

        public T Add(T item)
        {
            var it = _dbSet.Add(item);
            _context.SaveChanges();
            return it.Entity;
        }
        public void AddRange(IEnumerable<T> items)
        {
            _dbSet.AddRange(items);
            _context.SaveChanges();
        }

        public void Update(T item)
        {
            _context.Entry(item).State = EntityState.Modified;
            _context.SaveChanges();
        }
        public void UpdateRange(IEnumerable<T> items)
        {
            _context.UpdateRange(items);
            _context.SaveChanges();
        }
        public void Remove(T item)
        {
            _dbSet.Remove(item);
            _context.SaveChanges();
        }
    }

}
