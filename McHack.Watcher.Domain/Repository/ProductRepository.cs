﻿using Microsoft.EntityFrameworkCore;

namespace McHack.Domain.Repository
{
    public class ProductRepository : Repository<Product>
    {
        public ProductRepository(DbContext context) : base(context)
        {
        }
    }
}
