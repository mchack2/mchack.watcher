﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McHack.Domain.Repository
{
    public class CategoryRepository : Repository<ProductCategory>
    {
        public CategoryRepository(DbContext context) : base(context)
        {

        }
    }
}
