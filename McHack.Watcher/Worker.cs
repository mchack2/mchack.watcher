using McHack.Parser;
using McHack.Domain.Data;
using McHack.Domain.Repository;
using Microsoft.EntityFrameworkCore;

namespace McHack.Watcher
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly ManufacturerRepository _manufacturerRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly ProductRepository _productRepository;
        private readonly WebSiteSettingRepository _siteRepository;
        public Worker(ILogger<Worker> logger, IDbContextFactory<ManufacturerContext> contextFactory)
        {
            _logger = logger;

            var context = contextFactory.CreateDbContext();
            _manufacturerRepository = new ManufacturerRepository(context);
            _categoryRepository = new CategoryRepository(context);
            _productRepository = new ProductRepository(context);
            _siteRepository = new WebSiteSettingRepository(context);

        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                //�� ���������, ����� � ����
                var prCenterParser = new ProductCenterParser();

                await Task.Delay(24 * 60 * 60 * 1000, stoppingToken);

                await prCenterParser.Parse(_manufacturerRepository, _categoryRepository, 900);

            }
        }
    }
}