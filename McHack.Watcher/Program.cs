using McHack.Watcher;
using McHack.Domain.Data;
using Microsoft.EntityFrameworkCore;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((ctx, services) =>
    {
        IConfiguration configuration = ctx.Configuration;
        var connection = configuration.GetConnectionString("ManufacturerConnection");
        services.AddHostedService<Worker>()
                .AddEntityFrameworkNpgsql().AddDbContextFactory<ManufacturerContext>(options =>
                    options.UseNpgsql(connection, options => options.UseNetTopologySuite()
                ));
    })
    .UseDefaultServiceProvider(config=>
        config.ValidateScopes = false
    )
    .Build();

await host.RunAsync();
