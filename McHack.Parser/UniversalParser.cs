﻿using HtmlAgilityPack;
using McHack.Domain;
using McHack.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace McHack.Parser
{
    public class AutoProduct
    {
        public string Name { get; set; }
        public string ImageUrlSrc { get; set; }
        public string ImageUrlDataSrc { get; set; }
    }

    public class UniversalParser
    {
        //private string Url = @"https://wellsklad.ru/";
        //private string CatalogUrl = @"katalog/";
        //private string ExtendUrl = @"";
        private Regex trimer = new Regex("[^a-zA-Zа-яА-Я*]+");
        public IEnumerable<Product> Parse(Manufacturer manufacturer, WebSiteSettingRepository settingsRepository)
        {
            var web = new HtmlWeb();
            var settings = settingsRepository.Get().ToList();
            foreach (var setting in settings)
            {
                try
                {
                    var doc = web.Load(setting.RootUrl + setting.CatalogPart + setting.ExtendPart);
                    var node = doc.DocumentNode;
                    var productTags = node.SelectNodes($".//*[contains(@href, '{setting.CatalogPart}')]").ToList();
                    productTags = productTags.Where(x => x.GetAttributeValue<string>("href", "").Length > setting.CatalogPart.Length + 2).ToList();

                    var products = new List<AutoProduct>();
                    foreach (var tag in productTags)
                    {
                        var name = trimer.Replace(tag?.InnerText?.Trim() ?? "", " ").Trim();
                        if (string.IsNullOrWhiteSpace(name))
                            continue;

                        var imageSrc = tag.ParentNode?.ParentNode?
                            .SelectSingleNode($".//img")?
                            .GetAttributeValue("src", "");

                        var imageDataSrc = tag.ParentNode?.ParentNode?
                            .SelectSingleNode($".//img")?
                            .GetAttributeValue("data-src", "");

                        products.Add(new AutoProduct()
                        {
                            Name = name,
                            ImageUrlSrc = imageSrc,
                            ImageUrlDataSrc = imageDataSrc
                        });
                    }

                    var a = "";
                }
                catch(Exception ex)
                {

                }

            }
            return null;
        }
    }
}
