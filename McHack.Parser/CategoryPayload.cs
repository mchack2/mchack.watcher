﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McHack.Parser
{
    /// <summary>
    /// </summary>
    /// JSON модель результата запроса категорий. Каждое поле → html. ИМЕНА ПОЛЕЙ НЕ МЕНЯТЬ, СЕРИАЛИЗУЮТСЯ ПО ИМЕНИ!!!
    public class CategoryPayload
    {
       public string Region { get; set; }
       public string City { get; set; }
       public string Cat { get; set; }
       public string Scat { get; set; }
       public string Sscat { get; set; }
    }
}
