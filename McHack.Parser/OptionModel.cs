﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McHack.Parser
{
    public enum OptionType
    {
        Region,
        City,
        Cat,
        SCat,
        SSCat
    }

    public class OptionModel
    {
        public int Id { get; set; }
        public int ParseId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public OptionType Type { get; set; }
    }
}
