﻿using HtmlAgilityPack;
using McHack.Domain;
using McHack.Domain.Repository;
using System.Diagnostics;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;

namespace McHack.Parser
{
    public class ProductCenterParser
    {
        private static readonly string rootUrl = @"https://productcenter.ru";
        private static readonly string moscowManufactUrl = rootUrl + @"/producers/r-moskovskaia-obl-191/c-moskva-3109/";
        private static readonly string pagePattern = "page-{0}";

        private static readonly string productUrl = rootUrl + @"/products/producer-{0}-produkciya-{1}";

        private readonly Regex idRegex = new(@"\/[0-9]\d*\/");
        private int ExtractId(string url)
        {
            if (idRegex.IsMatch(url))
            {
                var match = idRegex.Match(url);
                var tar = match.Groups[0].Value;
                return int.Parse(tar[1..^1]);
            }
            else
            {
                throw new Exception();
            }
        }

        private readonly Regex nameRegex = new(@"([^\/]+$)");
        private string ExtractName(string url)
        {
            if (nameRegex.IsMatch(url))
            {
                return nameRegex.Match(url).Groups[0].Value;
            }
            else
            {
                throw new Exception();
            }
        }

        private static string NormilizeCategory(string category)
        {
            category = category.ToLower();
            category = Regex.Replace(category, @"[^a-zA-Zа-яА-Я ]+", string.Empty);
            category = category.Replace("категория ", string.Empty);
            category = category.Replace("категория", string.Empty);
            category = category.Replace(" гмосква", string.Empty);
            return category;
        }

        //private void Recursive(List<Category> categories, Product product, CategoryRepository categoryRepository)
        //{
        //    product.Category = 

        //}

        public async Task FillPricing(ProductRepository productRepository)
        {
            var products = productRepository.Get(x=>x.Price is null).ToList();

            var web = new HtmlWeb();
            var i = 0;
            var count = products.Count;
            foreach (var product in products)
            {
                var url = product.Url;
                var doc = web.Load(url);
                var statusCode = web.StatusCode;
                await Task.Delay(200);
                if(statusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine($"{++i}/{count}");

                    var priceValue = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(doc.DocumentNode.SelectSingleNode(".//div[@class='price']")?.InnerText?.Trim()));
                    productRepository.UpdatePrice(product.Id, priceValue);
                }
            }
        }

        private ProductCategory GetOrAddCategory(ProductCategory category, IEnumerable<ProductCategory> categories, CategoryRepository categoryRepository, ProductCategory parent = null)
        {
            var domain = categoryRepository.Get(x => x.Name == category.Name).FirstOrDefault()
                ?? categoryRepository.Add(new ProductCategory()
                {
                    Name = category.Name,
                    Parent = parent
                });

            var last = categories.FirstOrDefault(x => x.Parent == category);
            if (last != default)
                return GetOrAddCategory(last, categories, categoryRepository, domain);
            return domain;
        }

        public ProductCategory GetOrAddCategory(ProductCategory category, CategoryRepository categoryRepository)
        {
            var parent = default(ProductCategory);
            var childs = default(IEnumerable<ProductCategory>);
            if (category.Parent != null)
                parent = GetOrAddCategory(category.Parent, categoryRepository);
            
            childs = categoryRepository.Get(x=>x.Parent == parent);
            var cadegoryDb = childs.FirstOrDefault(c => c.Name == category.Name);
            if (cadegoryDb == null)
            {
                category.Parent = parent;
                cadegoryDb = categoryRepository.Add(category);
            }

            return cadegoryDb;
        }

        private Regex categoryRegex = new Regex(@"^(\/products\/catalog)");
        private async Task ParseDetails(Product product, CategoryRepository categoryRepository, int delayMs)
        {
            var web = new HtmlWeb();

            var url = product.Url;
            var doc = web.Load(url);
            var statusCode = web.StatusCode;

            await Task.Delay(delayMs);

            if (statusCode != HttpStatusCode.OK)
            {
                await Task.Delay(delayMs);
                doc = web.Load(url);
                statusCode = web.StatusCode;
            }

            if (statusCode == HttpStatusCode.OK)
            {
                var node = doc.DocumentNode;

                var crumbList = node.SelectSingleNode(".//ul[@class='crumbs_list']");
                var crumbs = crumbList.SelectNodes(".//li");
                var categories = new List<ProductCategory>();

                foreach (var crumb in crumbs)
                {
                    var atag = crumb.SelectSingleNode(".//a");
                    if (atag != null)
                    {
                        var href = atag.GetAttributeValue("href", string.Empty);
                        if (!string.IsNullOrWhiteSpace(href) && categoryRegex.Matches(href).Count > 0)
                            categories.Add(new ProductCategory() { Name = atag.InnerText.Trim() });
                    }
                }

                categories.Reverse();
                var j = 0;
                foreach (var category in categories.SkipLast(1))
                    category.Parent = categories[++j];

                product.Category = GetOrAddCategory(categories.First(), categoryRepository);

                var descriptionTag = node.SelectSingleNode(".//div[contains(@class, 'tc_description')]");
                var description = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(descriptionTag.InnerText.Trim()));

                var imgTag = node.SelectSingleNode(".//img[@itemprop='image']");
                var imgPart = imgTag.GetAttributeValue("src", "");
                var imgUrl = rootUrl + imgPart;

                var priceValue = node.SelectSingleNode(".//div[@class='price']")?.InnerText?.Trim(); ;
                product.Price = priceValue;
                product.Description = description;
                product.ImageUrl = imgUrl;                
            }
        }

        private async Task<ICollection<Product>> ParseProdcuts(Manufacturer manufacturer, CategoryRepository categoryRepository, int delayMs)
        {
            var web = new HtmlWeb();

            var manUrl = manufacturer.Url;
            var manId = ExtractId(manUrl);
            var manName = ExtractName(manUrl);

            var prUrl = string.Format(productUrl, manId, manName);

            var pageNum = 1;
            var docUrl = prUrl + "/" + string.Format(pagePattern, pageNum);
            var doc = web.Load(docUrl);
            var statusCode = web.StatusCode;

            await Task.Delay(delayMs);

            var products = new List<Product>();

            var productNum = 0;
            while (statusCode != HttpStatusCode.NotFound)
            {
                if (statusCode != HttpStatusCode.OK)
                {
                    doc = web.Load(docUrl);
                    await Task.Delay(delayMs);
                }

                var cards = doc.DocumentNode.SelectNodes("//div[contains(@class, 'card_item')]");
                foreach (var card in cards)
                {
                    try
                    {
                        Console.WriteLine($"Parse product by num: {++productNum} from page {pageNum}");
                        var product = new Product();

                        var atag = card.SelectSingleNode(".//a[@class='link']");
                        var href = atag.GetAttributeValue("href", "");
                        var url = rootUrl + href;

                        var name = atag.InnerText.Trim();
                        product.Name = name;
                        product.Url = url;

                        await ParseDetails(product, categoryRepository, delayMs);

                        products.Add(product);

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error! PageNum:{pageNum}, details:{ex.Message}.");
                    }
                }

                doc = web.Load(prUrl + string.Format(pagePattern, ++pageNum));
                statusCode = web.StatusCode;

                await Task.Delay(delayMs);
            }

            return products;
        }

        private const string ADDRESS_KEY = "address";
        private const string PHONE_KEY = "telephone";
        private const string EMAIL_KEY = "email";
        private const string URL_KEY = "url";

        private const string OFFICIAL_NAME_KEY = "Наименование";
        private const string OGRN_KEY = "ОГРН";
        private const string INN_KEY = "ИНН";
        private const string KPP_KEY = "КПП";
        private const string OFFICIAL_ADDRESS_KEY = "Юридический адрес";

        public async Task Parse(ManufacturerRepository manufacturerRepository, CategoryRepository categoryRepository, int delayMs)
        {
            var web = new HtmlWeb();

            var pageNum = 1;
            var docUrl = moscowManufactUrl + string.Format(pagePattern, pageNum);
            var doc = web.Load(docUrl);
            var statusCode = web.StatusCode;

            await Task.Delay(delayMs);

            var manufacturerNum = 0;
            while (statusCode != HttpStatusCode.NotFound)
            {
                var manufactCards = doc.DocumentNode.SelectNodes("//div[contains(@class, 'card_item')]");
                foreach (var manufactCard in manufactCards)
                {
                    Console.WriteLine($"Parse manufacturer by num: {++manufacturerNum} from page {pageNum}");
                    try
                    {
                        var atag = manufactCard.SelectSingleNode(".//a[@class='link']");
                        var href = atag.GetAttributeValue("href", "");
                        var url = rootUrl + href;

                        var manufacturer = new Manufacturer();
                        var name = atag.InnerText.Trim();

                        doc = web.Load(url);
                        var task = Task.Delay(delayMs);
                        var node = doc.DocumentNode;

                        var descriptionTag = node.SelectSingleNode(".//div[contains(@class, 'tc_description')]");
                        var description = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(descriptionTag.InnerText.Trim()));

                        var contactTag = node.SelectSingleNode(".//div[contains(@class, 'tc_contacts')]");


                        var address = contactTag.SelectSingleNode($".//*[@itemprop='{ADDRESS_KEY}']");
                        var phone = contactTag.SelectSingleNode($".//*[@itemprop='{PHONE_KEY}']");
                        var email = contactTag.SelectSingleNode($".//*[@itemprop='{EMAIL_KEY}']");
                        var webSite = contactTag.SelectSingleNode($".//*[@itemprop='{URL_KEY}']");

                        var officialName = contactTag.SelectSingleNode($".//tr[td[contains(text(), '{OFFICIAL_NAME_KEY}')]]")?.LastChild;
                        var ogrn = contactTag.SelectSingleNode($".//tr[td[contains(text(), '{OGRN_KEY}')]]")?.LastChild;
                        var inn = contactTag.SelectSingleNode($".//tr[td[contains(text(), '{INN_KEY}')]]")?.LastChild;
                        var kpp = contactTag.SelectSingleNode($".//tr[td[contains(text(), '{KPP_KEY}')]]")?.LastChild;
                        var officialAddress = contactTag.SelectSingleNode($".//tr[td[contains(text(), '{OFFICIAL_ADDRESS_KEY}')]]")?.LastChild;

                        manufacturer.Name = name;
                        manufacturer.Description = description;

                        manufacturer.Address = address?.InnerText?.Trim();
                        manufacturer.Phone = phone?.InnerText?.Trim();
                        manufacturer.Email = email?.InnerText?.Trim();
                        manufacturer.WebSite = webSite?.InnerText?.Trim();

                        manufacturer.OfficialName = officialName?.InnerText?.Trim();
                        manufacturer.OGRN = ogrn?.InnerText?.Trim();
                        manufacturer.INN = inn?.InnerText?.Trim();
                        manufacturer.KPP = kpp?.InnerText?.Trim();
                        manufacturer.OfficialAddress = officialAddress?.InnerText?.Trim();

                        manufacturer.Url = rootUrl + url;
                        manufacturer.Category = NormilizeCategory(manufactCard.SelectSingleNode(".//div[contains(@class, 'ii_category')]").GetAttributeValue("title", ""));
                        manufacturer.Products = await ParseProdcuts(manufacturer, categoryRepository, delayMs);

                        manufacturerRepository.Add(manufacturer);

                        await task;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error! PageNum:{pageNum}, details:{ex.Message}.");
                    }
                }

                doc = web.Load(moscowManufactUrl + string.Format(pagePattern, ++pageNum));
                statusCode = web.StatusCode;
            }
        }
    }
}